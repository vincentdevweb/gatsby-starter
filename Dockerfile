
FROM node AS build

WORKDIR /appgatsby

COPY . .

RUN npm install -g gatsby-cli
RUN npm install
RUN gatsby build

# Exécuter Gatsby en mode développement
#ENTRYPOINT ["gatsby", "develop","-H","0.0.0.0"]

##PRENDS LE DOSSIER DOCKER ET LE MET DANS UN SERVEUR
FROM nginx AS deploy
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /appgatsby/public .
ENTRYPOINT [ "nginx","-g","daemon off;" ]